package com.centroid.deliveryboyapp.Constants;

import android.content.Context;

public class Constants {


    public static String url="https://centroidsoftwares.com/99_villagemart/deliveryapi/";

    public static String imgurl="https://centroidsoftwares.com/99_villagemart/images/";

    public static String tokenkey="tokenkey";

    public static final String BASEURL = "https://centroidsoftwares.com/99_villagemart/api/";
    public static final String SHAREDPREF = "MYPREF";
    public static final String USERTOKEN = "TOKEN";

    public static final String USERBRANCH = "USERBRANCH";

    public static final String SEARCHTOKEN = "SEARCHTOKEN";

    public static final String LASTADDDRESS = "LASTADDDRESS";
    public static final String CURRENTORDER = "CURRENTORDER";


    public static final String CURRENTUSER = "CURRENTUSER";

    public static final String DELIVERYBOY = "DELIVERYBOY";

    public static final String OFFICEBOY = "OFFICEBOY";



 public String getTempstorageFilePath(Context context) {

     Long tsLong = System.currentTimeMillis()/1000;
     String ts = tsLong.toString();

     String tempstorage = context.getExternalCacheDir()+"/"+ts+".png";

     return tempstorage;

 }

}
