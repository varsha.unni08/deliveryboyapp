package com.centroid.deliveryboyapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.OrderDate
import kotlinx.android.synthetic.main.activity_my_orders.*
import kotlinx.android.synthetic.main.layout_maincategory_adapter.view.*
import java.lang.Exception
import java.text.SimpleDateFormat

class OrderDateAdapter(set: ArrayList<OrderDate>, context: Context) : RecyclerView.Adapter<OrderDateAdapter.OrderDateHolder>() {


   var s=set;

   var cont=context

    class OrderDateHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDateHolder {

        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_maincategory_adapter,parent,false)


        return OrderDateHolder(v)

    }

    override fun getItemCount(): Int {

        return s.size
    }

    override fun onBindViewHolder(holder: OrderDateHolder, position: Int) {

        try {
            val format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.parse(s.get(position).date)


            val format_req = SimpleDateFormat("dd-MM-yyyy")

            val d = format_req.format(date)



            holder.itemView.txtData.setText(d.toString())
        }catch (Ex: Exception)
        {

        }




        if(s.get(position).selected==1)
        {

            holder.itemView.img_arrowkey.setImageResource(R.drawable.ic_arrow_drop_up)
            holder.itemView.recyclerview_subcat.visibility=View.VISIBLE

            holder.itemView.recyclerview_subcat.layoutManager=LinearLayoutManager(cont)
            var salesOrderAdapter= OrderHistoryAdapter(cont,s.get(position).salesOrders)
            holder.itemView.recyclerview_subcat.adapter=salesOrderAdapter
        }
        else
        {
            holder.itemView.img_arrowkey.setImageResource(R.drawable.ic_arrow_drop_down)
            holder.itemView.recyclerview_subcat.visibility=View.GONE

        }



        holder.itemView.txtData.setOnClickListener {

            if(s.get(position).selected==0) {

                for (i in s) {

                    i.selected = 0

                }


                s.get(position).selected = 1;
            }
            else{

                for (i in s) {

                    i.selected = 0

                }

            }

            notifyItemRangeChanged(0,s.size)

        }

        holder.itemView.img_arrowkey.setOnClickListener {

            if(s.get(position).selected==0) {

                for (i in s) {

                    i.selected = 0

                }


                s.get(position).selected = 1;
            }
            else{

                for (i in s) {

                    i.selected = 0

                }

            }

            notifyItemRangeChanged(0,s.size)

        }







    }
}