package com.centroid.deliveryboyapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.Views.BillPrintActivity
import com.centroid.deliveryboyapp.Views.SalesOrderDetailsActivity
import com.centroid.deliveryboyapp.domain.SalesOrder
import kotlinx.android.synthetic.main.layout_salesorder.view.*
import kotlinx.android.synthetic.main.list_itemorders.view.*
import kotlinx.android.synthetic.main.list_itemorders.view.ServiceCharge
import kotlinx.android.synthetic.main.list_itemorders.view.amount
import kotlinx.android.synthetic.main.list_itemorders.view.name
import kotlinx.android.synthetic.main.list_itemorders.view.orderdate
import kotlinx.android.synthetic.main.list_itemorders.view.totalamount
import kotlinx.android.synthetic.main.list_itemorders.view.txtTransactiontype
import java.lang.Exception
import java.text.SimpleDateFormat

class SalesDetailOrderAdapter(context: Context, list:List<SalesOrder>):RecyclerView.Adapter<SalesDetailOrderAdapter.SalesDetailOrderholder>() {

    var cont:Context=context
    var  li:List<SalesOrder> = list

    public class SalesDetailOrderholder(itemView: View) : RecyclerView.ViewHolder(itemView){


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalesDetailOrderholder {
        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_salesorder,parent,false)

        return SalesDetailOrderholder(v)
    }

    override fun getItemCount(): Int {

        return li.size
    }

    override fun onBindViewHolder(holder: SalesDetailOrderholder, position: Int) {

        holder.itemView.name.setText("Order id : "+li.get(position).cdSalesOrder)
        //  holder.itemView.orderdate.setText(""+li.get(position).dtSalesOrder)



        try {
            val format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.parse(li.get(position).dtDelivery)


            val format_req = SimpleDateFormat("dd-MM-yyyy")

            val d = format_req.format(date)

            holder.itemView.orderdate.setText(""+d)


        }catch (Ex: Exception)
        {

        }








        holder.itemView.amount.setText(li.get(position).vlTotal +cont.getString(R.string.rs))
        holder.itemView.ServiceCharge.setText(li.get(position).vlDeliveryCharge +cont.getString(R.string.rs))


        var a=li.get(position).vlTotal.toDouble()+li.get(position).vlDeliveryCharge.toDouble()
        holder.itemView.totalamount.setText(a.toString()+" " +cont.getString(R.string.rs))


        if(li.get(position).cdTransactionType.equals("0")) {
            holder.itemView.txtTransactiontype.setText("Cash on delivery")
        }
        else{

            holder.itemView.txtTransactiontype.setText("Online")
        }

        holder.itemView.setOnClickListener {

            if(li.get(position).cd_checked.equals("0")) {

                val intent = Intent(cont, SalesOrderDetailsActivity::class.java)
                intent.putExtra("Order", li.get(position))

                cont.startActivity(intent)
            }
            else{

                Toast.makeText(cont,"You already checked these items",Toast.LENGTH_SHORT).show()
            }
        }


        if(li.get(position).cd_checked.equals("0"))
        {
           holder.itemView.layout_checked.visibility=View.GONE
            holder.itemView.btnprint.visibility=View.GONE

        }
        else{

            holder.itemView.layout_checked.visibility=View.VISIBLE
            holder.itemView.btnprint.visibility=View.VISIBLE
        }


        holder.itemView.btnprint.setOnClickListener {

            cont.startActivity(Intent(cont,
                BillPrintActivity::class.java).putExtra("salesorder",li.get(position).cdSalesOrder))

        }
    }
}