package com.centroid.deliveryboyapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.SalesOrderItem
import kotlinx.android.synthetic.main.layout_salesorderitem.view.*
import kotlinx.android.synthetic.main.layout_salesorderitem.view.img
import kotlinx.android.synthetic.main.layout_salesorderitem.view.name
import kotlinx.android.synthetic.main.list_salesorderitem.view.*

class SalesUserItemAdapter(context: Context, list:List<SalesOrderItem>):RecyclerView.Adapter<SalesUserItemAdapter.SalesOrderItemholder> (){

    var cont:Context=context
    var  li:List<SalesOrderItem> = list


    public class SalesOrderItemholder(itemView: View) : RecyclerView.ViewHolder(itemView){


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalesOrderItemholder {

        var v= LayoutInflater.from(parent.context).inflate(R.layout.list_salesorderitem,parent,false)


        return SalesOrderItemholder(v)

    }

    override fun getItemCount(): Int {

return li.size

    }

    override fun onBindViewHolder(holder: SalesOrderItemholder, i: Int) {

        Glide.with(cont).applyDefaultRequestOptions(
            RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher)
        ).load(Constants.imgurl + li.get(i).itemImage)
            .into(holder.itemView.img)

        if(li.get(i).checked==0)
        {

            holder.itemView.checkbox.isChecked=false
        }
        else{

            holder.itemView.checkbox.isChecked=true

        }


        holder.itemView.name.setText(""+li.get(i).itemName+"\nQuantity : "+li.get(i).nrQty+""+"\nUnit price :"+li.get(i).unitprice+" "+cont.getString(R.string.rs)+"\nNet total :"+li.get(i).vlNetTotal+" "+cont.getString(R.string.rs))

        holder.itemView.checkbox.setOnClickListener {

            if(holder.itemView.checkbox.isChecked)
            {

                li.get(i).checked=1

            }
            else{

                li.get(i).checked=0

            }

            notifyItemChanged(i)

        }
    }
}