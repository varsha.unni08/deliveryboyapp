package com.centroid.deliveryboyapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.SalesItems
import com.centroid.deliveryboyapp.domain.SalesOrderItem
import kotlinx.android.synthetic.main.layout_saleitem.view.*

class SalesOrderItemBillAdapter(context: Context, list:List<SalesItems>):RecyclerView.Adapter<SalesOrderItemBillAdapter.SalesOrderItemBillholder>() {


    var cont=context
    var li=list

    public class SalesOrderItemBillholder(itemView: View) : RecyclerView.ViewHolder(itemView){


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalesOrderItemBillholder {


        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_saleitem,parent,false)


        return SalesOrderItemBillholder(v)

    }

    override fun getItemCount(): Int {

        return li.size
    }

    override fun onBindViewHolder(holder: SalesOrderItemBillholder, position: Int) {

        holder.itemView.txtitemname.setText(li.get(position).item.dsItem)

        holder.itemView.txtQty.setText(li.get(position).nrQty)
        holder.itemView.txtUnitprice.setText(li.get(position).item.vlSellingPrice+cont.getString(R.string.rs))
        holder.itemView.txtTotal.setText(li.get(position).vlTotal+cont.getString(R.string.rs))

    }
}