package com.centroid.deliveryboyapp.adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.Views.MainActivity
import com.centroid.deliveryboyapp.domain.SalesOrder
import kotlinx.android.synthetic.main.list_itemorders.view.*
import java.lang.Exception
import java.text.SimpleDateFormat


class SalesOrderAdapter(context:Context,list:List<SalesOrder>) : RecyclerView.Adapter<SalesOrderAdapter.Chatroomholder> (){

   var cont:Context=context
   var  li:List<SalesOrder> = list
    public class Chatroomholder(itemView: View ) : RecyclerView.ViewHolder(itemView){


    }


     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Chatroomholder {



         var v= LayoutInflater.from(parent.context).inflate(R.layout.list_itemorders,parent,false)

         return     Chatroomholder(v)

     }

     override fun getItemCount(): Int {


   return li.size

     }

     override fun onBindViewHolder(holder: Chatroomholder, position: Int) {

         holder.itemView.name.setText("Order id : "+li.get(position).cdSalesOrder)
       //  holder.itemView.orderdate.setText(""+li.get(position).dtSalesOrder)



         try {
             val format = SimpleDateFormat("yyyy-MM-dd")
             val date = format.parse(li.get(position).dtDelivery)


             val format_req = SimpleDateFormat("dd-MM-yyyy")

             val d = format_req.format(date)

             holder.itemView.orderdate.setText(""+d)


         }catch (Ex: Exception)
         {

         }








         holder.itemView.amount.setText(li.get(position).vlTotal +cont.getString(R.string.rs))
         holder.itemView.ServiceCharge.setText(li.get(position).vlDeliveryCharge +cont.getString(R.string.rs))


         var a=li.get(position).vlTotal.toDouble()+li.get(position).vlDeliveryCharge.toDouble()
         holder.itemView.totalamount.setText(a.toString()+" " +cont.getString(R.string.rs))


         if(li.get(position).cdTransactionType.equals("0")) {
             holder.itemView.txtTransactiontype.setText("Cash on delivery")
         }
         else{

             holder.itemView.txtTransactiontype.setText("Online")
         }


         holder.itemView.btnpick.setOnClickListener {


             ( cont as MainActivity).changePickStatus(li.get(position))





         }



              }
 }
