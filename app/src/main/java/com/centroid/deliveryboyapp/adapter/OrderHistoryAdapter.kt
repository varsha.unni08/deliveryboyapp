package com.centroid.deliveryboyapp.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.SalesOrder
import kotlinx.android.synthetic.main.layout_maincategory_adapter.view.*
import kotlinx.android.synthetic.main.list_itemorders.view.ServiceCharge
import kotlinx.android.synthetic.main.list_itemorders.view.amount
import kotlinx.android.synthetic.main.list_itemorders.view.name
import kotlinx.android.synthetic.main.list_itemorders.view.orderdate
import kotlinx.android.synthetic.main.list_itemorders.view.totalamount
import kotlinx.android.synthetic.main.list_itemorders.view.txtRefunded
import kotlinx.android.synthetic.main.list_itemorders.view.txtTransactiontype
import java.lang.Exception
import java.text.SimpleDateFormat

class OrderHistoryAdapter(context: Context, list:List<SalesOrder>) :RecyclerView.Adapter<OrderHistoryAdapter.OrderHistoryholder>() {


    var cont:Context=context
    var  li:List<SalesOrder> = list
    public class OrderHistoryholder(itemView: View) : RecyclerView.ViewHolder(itemView){


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderHistoryholder {

        var v= LayoutInflater.from(parent.context).inflate(R.layout.layout_orderhistory,parent,false)


        return OrderHistoryholder(v)

    }

    override fun getItemCount(): Int {

        return li.size
    }

    override fun onBindViewHolder(holder: OrderHistoryholder, position: Int) {

        holder.itemView.name.setText("Order id : "+li.get(position).cdSalesOrder)




        try {
            val format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.parse(li.get(position).dtDelivery)


            val format_req = SimpleDateFormat("dd-MM-yyyy")

            val d = format_req.format(date)

            holder.itemView.orderdate.setText(""+d)


        }catch (Ex: Exception)
        {

        }











        holder.itemView.amount.setText(li.get(position).vlTotal +cont.getString(R.string.rs))
        holder.itemView.ServiceCharge.setText(li.get(position).vlDeliveryCharge +cont.getString(R.string.rs))


        var a=li.get(position).vlTotal.toDouble()+li.get(position).vlDeliveryCharge.toDouble()
        holder.itemView.totalamount.setText(a.toString()+" " +cont.getString(R.string.rs))


        if(li.get(position).cdTransactionType.equals("0")) {
            holder.itemView.txtTransactiontype.setText("Cash on delivery")
        }
        else{

            holder.itemView.txtTransactiontype.setText("Online")
        }

        if(li.get(position).cdSalesOrderStatus.equals("3")) {
            holder.itemView.txtRefunded.setText("Cancelled")

            holder.itemView.txtRefunded.setTextColor(Color.RED)
        }
        else if(li.get(position).cdSalesOrderStatus.equals("5")){

            holder.itemView.txtRefunded.setText("Delivered")

            holder.itemView.txtRefunded.setTextColor(Color.parseColor("#7fb546"))
        }



    }
}