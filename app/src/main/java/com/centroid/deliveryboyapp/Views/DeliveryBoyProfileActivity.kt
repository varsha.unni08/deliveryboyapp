package com.centroid.deliveryboyapp.Views

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.adapter.SalesOrderAdapter
import com.centroid.deliveryboyapp.domain.DeliveryBoy
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_delivery_boy_profile.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeliveryBoyProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_boy_profile)
        supportActionBar!!.hide()

imgback.setOnClickListener {

    onBackPressed()
}



        cardlogout.setOnClickListener {


            val builder = AlertDialog.Builder(this)
            builder.setTitle("Near a job")
            builder.setMessage("Do you want to logout now ?")
            builder.setNegativeButton(
                "yes"
            ) {


                    dialog, which -> dialog.dismiss()

                PreferenceHelper(this).putData(Constants.tokenkey,"")
                PreferenceHelper(this).putData(Constants.CURRENTUSER,"")
                PreferenceHelper(this).putData(Constants.CURRENTORDER,"")
                finishAffinity()
                System.exit(0)


            }


            builder.setPositiveButton(
                "no"
            ) {


                    dialog, which -> dialog.dismiss()


            }
            val alertDialog = builder.create()
            alertDialog.show()


        }


        cardorders.setOnClickListener {

            startActivity(Intent(this, MyOrdersActivity::class.java))
        }

        showProfile()
    }



    fun showProfile()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<DeliveryBoy> = retrofithelper.getClient().getDeliveryBoyProfile(
            "Bearer "+ PreferenceHelper(this).getData(Constants.tokenkey)
        );

        a.enqueue(object : Callback<DeliveryBoy> {
            override fun onResponse(call: Call<DeliveryBoy>, response: Response<DeliveryBoy>) {



                if (response.body() != null) {

                    var p = response.body();

                  username.setText(p!!.dsBoyName)
                    mobile.setText(p!!.mobileNo)
                    email.setText(p!!.email)
                    dob.setText(p!!.dsAddress)





                }
                else{


                }


            }

            override fun onFailure(call: Call<DeliveryBoy>, t: Throwable) {



                if (!DownloadUtils().isNetworkConnected(this@DeliveryBoyProfileActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@DeliveryBoyProfileActivity);
                }

                t.printStackTrace()


            }
        })
    }
}
