package com.centroid.deliveryboyapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getSystemService
import android.content.DialogInterface

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService


class SplashActivity : AppCompatActivity() {

    var handler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()


        if (PreferenceHelper(this).getData(Constants.CURRENTUSER).equals("")) {


            var s = arrayOf("Delivery", "Sales")


            AlertDialog.Builder(this)
                .setCancelable(false)
                .setSingleChoiceItems(s, 0, null)
                .setPositiveButton("ok",
                    DialogInterface.OnClickListener { dialog, whichButton ->
                        dialog.dismiss()

                        val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition

                        val si = s[selectedPosition]


                        PreferenceHelper(this).putData(Constants.CURRENTUSER, si)

                        if (selectedPosition == 0) {


                            startActivity(Intent(this, LoginActivity::class.java))
                            finish()


                        } else {


                            startActivity(Intent(this, SalesLoginActivity::class.java))
                            finish()

                        }


                    })
                .show()


        } else {


            handler = Handler(Looper.getMainLooper())

            handler!!.postDelayed(Runnable {


                if (PreferenceHelper(this).getData(Constants.CURRENTUSER).equals("Delivery")) {

                    if (!PreferenceHelper(this).getData(Constants.tokenkey).equals("")) {
                        if (!PreferenceHelper(this).getData(Constants.CURRENTORDER).equals("")) {


                            startActivity(Intent(this, OrderDetailsActivity::class.java))
                            finish()

                        } else {


                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        }
                    } else {

                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()

                    }






                }

                if (PreferenceHelper(this).getData(Constants.CURRENTUSER).equals("Sales")) {


                    if (!PreferenceHelper(this).getData(Constants.tokenkey).equals("")) {



                            startActivity(Intent(this, SaleDetailsActivity::class.java))
                            finish()

                    } else {

                        startActivity(Intent(this, SalesLoginActivity::class.java))
                        finish()

                    }
                }

            }, 3000)

        }
    }
}
