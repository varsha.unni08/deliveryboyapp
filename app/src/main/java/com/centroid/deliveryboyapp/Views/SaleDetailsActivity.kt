package com.centroid.deliveryboyapp.Views

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.adapter.SalesDetailOrderAdapter
import com.centroid.deliveryboyapp.adapter.SalesOrderAdapter
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SaleDetailsActivity : AppCompatActivity() {

    var count=0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale_details)
        supportActionBar!!.hide()
        showLatestOrders()
        val timer = Timer()
        val hourlyTask = object : TimerTask() {
            override  fun run() {


                showLatestOrders()

            }
        }


        timer.schedule (hourlyTask, 2000, 3000);
    }

    override fun onRestart() {
        super.onRestart()
        showLatestOrders()
    }




    fun showLatestOrders()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<SalesOrder>> = retrofithelper.getClient().getSalesOrder(
            "Bearer "+ PreferenceHelper(this).getData(Constants.tokenkey)
        );

        a.enqueue(object : Callback<List<SalesOrder>> {
            override fun onResponse(call: Call<List<SalesOrder>>, response: Response<List<SalesOrder>>) {



                if (response.body() != null) {

                    var p = response.body();

                    if(p!!.size>0)
                    {



                        if(count<p!!.size)
                        {

                            sendPushNotification("You have a new order")
                        }
                        count=p!!.size

                        recycler_view.layoutManager=LinearLayoutManager(this@SaleDetailsActivity)
                        var salesOrderAdapter= SalesDetailOrderAdapter(this@SaleDetailsActivity,p)
                        recycler_view.adapter=salesOrderAdapter


                    }






                }
                else{


                }


            }

            override fun onFailure(call: Call<List<SalesOrder>>, t: Throwable) {



                if (!DownloadUtils().isNetworkConnected(this@SaleDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@SaleDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }


    private fun sendPushNotification(msg: String) {

        try {


            val m = (Date().time / 1000L % Integer.MAX_VALUE).toInt()


            val CHANNEL_ID = "my_channel_01"// The id of the channel.
            val name = "mychannel"// The user-visible name of the channel.
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel: NotificationChannel
            // Create a notification and set the notification channel.

            var notification: Notification? = null

            val mNotificationManager: NotificationManager

            val intent = Intent(this@SaleDetailsActivity, SplashActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            //            builder.setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification = Notification.Builder(this@SaleDetailsActivity, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_sales)
                    .setContentIntent(pendingIntent)


                    .setChannelId(CHANNEL_ID)
                    .setStyle(Notification.BigTextStyle().bigText(msg))
                    .build()

                mChannel = NotificationChannel(CHANNEL_ID, name, importance)
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager.createNotificationChannel(mChannel)

            } else {

                notification = Notification.Builder(this@SaleDetailsActivity)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_sales)
                    .setContentIntent(pendingIntent)
                    .setStyle(Notification.BigTextStyle().bigText(msg))

                    .build()
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }


            // Issue the notification.
            mNotificationManager.notify(m, notification)


            val mp: MediaPlayer
            mp = MediaPlayer.create(this@SaleDetailsActivity, R.raw.notifysound)
            mp.setOnCompletionListener { mp ->
                var mp = mp
                // TODO Auto-generated method stub
                mp!!.reset()
                mp!!.release()
                mp = null
            }
            mp.start()



            //
            //
        } catch (e: Exception) {

        }


    }
}
