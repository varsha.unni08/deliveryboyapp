package com.centroid.deliveryboyapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.hide()


        btnlogin.setOnClickListener {

            if(!edtPhone.text.toString().equals("")) {

                if(edtPhone.text.toString().length==10) {


try {

    var p: ProgressFragment = ProgressFragment()
    p.show(supportFragmentManager, "dhjjd")


    val retrofithelper = Retrofithelper()

    var a: Call<JsonObject> = retrofithelper.getClient().UserLogin(
        edtPhone!!.text.toString().trim()
    )


    a.enqueue(object : Callback<JsonObject> {
        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

            p.dismiss()

            if (response.body() != null) {

                var p = response.body();

                var jsonobj=JSONObject(p.toString());

                if (jsonobj.getInt("status")==1) {

                   // PreferenceHelper(this@LoginActivity).putData(Constants.tokenkey, p!!.token)

                  var token=  jsonobj.getString("token")

                    startActivity(Intent(this@LoginActivity, OTPActivity::class.java).putExtra("token",token))
                    finish()


                } else {

                    Toast.makeText(this@LoginActivity, "Login failed", Toast.LENGTH_SHORT).show()


                }


            }


        }

        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            p.dismiss()

            if (!DownloadUtils().isNetworkConnected(this@LoginActivity)) {
                DownloadUtils().showNoConnectiondialog(this@LoginActivity);
            }

            t.printStackTrace()


        }
    })


}catch (ex:Exception)
{

}






                }
                else{

                    Toast.makeText(
                        this,
                        "Please enter valid phone number",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
            else{

                Toast.makeText(
                    this,
                    "Please enter phone number",
                    Toast.LENGTH_SHORT
                ).show()

            }

        }
    }
}
