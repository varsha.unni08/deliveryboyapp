package com.centroid.deliveryboyapp.Views

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main2.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService

import java.util.*

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat.getSystemService
import com.centroid.deliveryboyapp.adapter.SalesOrderAdapter
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.google.gson.Gson

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.media.MediaPlayer
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService








class MainActivity : AppCompatActivity() {

    var handler: Handler?=null

    var count=0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        supportActionBar!!.hide()


        recycler_view.layoutManager=LinearLayoutManager(this)!!

        handler=Handler(Looper.getMainLooper())

        showLatestOrders()

        imgUser.setOnClickListener {


            startActivity(Intent(this, DeliveryBoyProfileActivity::class.java))


        }

        val timer = Timer()
        val hourlyTask = object : TimerTask() {
          override  fun run() {


              showLatestOrders()

            }
        }


        timer.schedule (hourlyTask, 2000, 3000);

    }


    override fun onRestart() {
        super.onRestart()
        showLatestOrders()
    }




    fun showLatestOrders()
    {
        val retrofithelper = Retrofithelper()

        var a: Call<List<SalesOrder>> = retrofithelper.getClient().getOrders(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey)
        );

        a.enqueue(object : Callback<List<SalesOrder>> {
            override fun onResponse(call: Call<List<SalesOrder>>, response: Response<List<SalesOrder>>) {



                if (response.body() != null) {

                    var p = response.body();

                    if(p!!.size>0)
                    {



                        if(count<p!!.size)
                        {

                            sendPushNotification("You have a new order")
                        }
                        count=p!!.size

                        var salesOrderAdapter=SalesOrderAdapter(this@MainActivity,p)
                        recycler_view.adapter=salesOrderAdapter


                    }






                }
                else{


                }


            }

            override fun onFailure(call: Call<List<SalesOrder>>, t: Throwable) {



                if (!DownloadUtils().isNetworkConnected(this@MainActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@MainActivity);
                }

                t.printStackTrace()


            }
        })
    }


    private fun sendPushNotification(msg: String) {

        try {


            val m = (Date().time / 1000L % Integer.MAX_VALUE).toInt()


            val CHANNEL_ID = "my_channel_01"// The id of the channel.
            val name = "mychannel"// The user-visible name of the channel.
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel: NotificationChannel
            // Create a notification and set the notification channel.

            var notification: Notification? = null

            val mNotificationManager: NotificationManager

            val intent = Intent(this@MainActivity, SplashActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            //            builder.setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification = Notification.Builder(this@MainActivity, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_sales)
                    .setContentIntent(pendingIntent)


                    .setChannelId(CHANNEL_ID)
                    .setStyle(Notification.BigTextStyle().bigText(msg))
                    .build()

                mChannel = NotificationChannel(CHANNEL_ID, name, importance)
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager.createNotificationChannel(mChannel)

            } else {

                notification = Notification.Builder(this@MainActivity)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_sales)
                    .setContentIntent(pendingIntent)
                    .setStyle(Notification.BigTextStyle().bigText(msg))

                    .build()
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }


            // Issue the notification.
            mNotificationManager.notify(m, notification)


            val mp: MediaPlayer
            mp = MediaPlayer.create(this@MainActivity, R.raw.notifysound)
            mp.setOnCompletionListener { mp ->
                var mp = mp
                // TODO Auto-generated method stub
                mp!!.reset()
                mp!!.release()
                mp = null
            }
            mp.start()



            //
            //
        } catch (e: Exception) {

        }


    }


    fun changePickStatus(order:SalesOrder)
    {

        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().ChangepickupStatus(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order.cdSalesOrder,"1"
        );



        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    var jsonobj=JSONObject(p.toString());

                    if (jsonobj.getInt("status")==1) {


                        val gson = Gson()
                        val jsonString = gson.toJson(order)


                        PreferenceHelper(this@MainActivity).putData(Constants.CURRENTORDER,jsonString)
                        startActivity(Intent(this@MainActivity, OrderDetailsActivity::class.java))



                    } else {

                        Toast.makeText(this@MainActivity, "failed", Toast.LENGTH_SHORT).show()


                    }


                }


            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@MainActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@MainActivity);
                }

                t.printStackTrace()


            }
        })
    }
}
