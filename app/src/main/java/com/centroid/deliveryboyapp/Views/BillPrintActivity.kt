package com.centroid.deliveryboyapp.Views

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CancellationSignal
import android.os.ParcelFileDescriptor
import androidx.core.content.ContextCompat
import com.centroid.deliveryboyapp.R
import kotlinx.android.synthetic.main.activity_print_sample.*

import android.content.Context
import android.print.*
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.adapter.SalesOrderItemBillAdapter
import com.centroid.deliveryboyapp.adapter.SalesUserItemAdapter
import com.centroid.deliveryboyapp.domain.SalesOrderBill
import com.centroid.deliveryboyapp.domain.SalesOrderItem
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.itextpdf.kernel.pdf.PdfDocument


import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.property.TextAlignment
import com.itextpdf.layout.property.UnitValue

import kotlinx.android.synthetic.main.activity_print_sample.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*


class BillPrintActivity : AppCompatActivity() {

    var filename="";

    var salesOrder:String?="0"

    var sb:SalesOrderBill?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_sample)
        supportActionBar!!.hide()

        salesOrder=intent.getStringExtra("salesorder")

        filename=this.externalCacheDir.toString()+"/"+salesOrder+".pdf"

        generateBill()
        imgback.setOnClickListener {


            onBackPressed()
        }

        btnprint.setOnClickListener {


            if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
            {

                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),11)

            }
            else{


                var fi=File(filename)

                if (!fi.exists())
                {
                    fi.createNewFile()
                }




                var outputStream: OutputStream? = null

                outputStream=FileOutputStream(fi)

               var pw= PdfWriter(outputStream);


                var document =  Document(PdfDocument(pw));


                document.add( Paragraph("RETAIL INVOICE").setBold().setUnderline().setTextAlignment(
                    TextAlignment.CENTER));



                document.add( Paragraph("Oreder Id : "+sb!!.cdSalesOrder));
                document.add( Paragraph("Customer : "+sb!!.user.dsCustomer));
                document.add( Paragraph("Mobile : "+sb!!.user.dsPhoneno));




var table =  Table(UnitValue.createPointArray( floatArrayOf(180f, 30f, 80f, 80f)));

    // headers
    table.addCell( Paragraph("Item name").setBold());
    table.addCell( Paragraph("Quantity").setBold());
    table.addCell( Paragraph("Unit price").setBold());
    table.addCell( Paragraph("Total price").setBold());

 for(a in sb!!.salesorderitem)
    {
        table.addCell( Paragraph(a.item.dsItem+""));
        table.addCell( Paragraph(a.nrQty));
        table.addCell( Paragraph(a.item.vlSellingPrice+""));
        table.addCell( Paragraph(a.vlTotal+""));

    }

                var b=sb!!.vlTotal.toDouble()+sb!!.vlDeliveryCharge.toDouble()




                document.add( Paragraph("Amount : "+sb!!.vlTotal+getString(R.string.rs)+"\nDelivery charge : "+sb!!.vlDeliveryCharge+""+getString(R.string.rs)+"\nTotal amount : "+b+getString(R.string.rs)));
    document.add(table);

                document.close()


                val printManager = this
                    .getSystemService(Context.PRINT_SERVICE) as PrintManager

                // Set job name, which will be displayed in the print queue
                val jobName =this.getString(com.centroid.deliveryboyapp.R.string.app_name)+salesOrder + " Document"

                // Start a print job, passing in a PrintDocumentAdapter implementation
                // to handle the generation of a print document
                printManager.print(
                    jobName, MyPrintDocumentAdapter(filename),
                    null
                ) //

            }





        }
    }


    class MyPrintDocumentAdapter(file:String):PrintDocumentAdapter()
    {

        var f=file



        override fun onLayout(
            p0: PrintAttributes?,
            p1: PrintAttributes?,
            p2: CancellationSignal?,
            p3: LayoutResultCallback?,
            p4: Bundle?
        ) {


            val info = PrintDocumentInfo.Builder(File(f).name)
                .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                .setPageCount(PrintDocumentInfo.PAGE_COUNT_UNKNOWN)
                .build()

            p3!!.onLayoutFinished(info, p0 != p1)

        }

        override fun onWrite(
            p0: Array<out PageRange>?,
            p1: ParcelFileDescriptor?,
            p2: CancellationSignal?,
            p3: WriteResultCallback?
        ) {

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null


            try {





                inputStream = FileInputStream(File(f))
                outputStream = FileOutputStream(p1!!.fileDescriptor)

                inputStream!!.copyTo(outputStream)

                if (p2!!.isCanceled) {
                    p3!!.onWriteCancelled()
                } else {
                    p3!!.onWriteFinished(arrayOf(PageRange.ALL_PAGES))
                }
            } catch (ex: Exception) {
                p3!!.onWriteFailed(ex.message)
                Log.e("PDFDocumentAdapter", "Could not write: ${ex.localizedMessage}")
            } finally {
                inputStream?.close()
                outputStream?.close()
            }





        }
    }




    fun generateBill() {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<SalesOrderBill> = retrofithelper.getClient().getSalesOrderFullDetails(
            "Bearer " + PreferenceHelper(this).getData(Constants.tokenkey), salesOrder!!
        );


        a.enqueue(object : Callback<SalesOrderBill> {
            override fun onResponse(call: Call<SalesOrderBill>, response: Response<SalesOrderBill>) {

                p.dismiss()

                if (response.body() != null) {

                    var s=response.body()

                    sb=response.body()

                    txtOrderid.setText("Order id : "+s!!.cdSalesOrder+"\n"+s!!.user.dsCustomer+"\n"+s!!.dsDeliveryAddress)

                    orderdate.setText(""+s!!.dtDelivery)

                    var b=s!!.vlTotal.toDouble()+s!!.vlDeliveryCharge.toDouble()

                    totalprice.setText("Amount : "+s!!.vlTotal+getString(R.string.rs)+"\nDelivery charge : "+s!!.vlDeliveryCharge+""+getString(R.string.rs)+"\nTotal amount : "+b+getString(R.string.rs))



                    recycler_view.layoutManager=LinearLayoutManager(this@BillPrintActivity)

                    recycler_view.adapter=SalesOrderItemBillAdapter(this@BillPrintActivity,s!!.salesorderitem)

                }


            }

            override fun onFailure(call: Call<SalesOrderBill>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@BillPrintActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@BillPrintActivity);
                }

                t.printStackTrace()


            }
        })

    }
}
