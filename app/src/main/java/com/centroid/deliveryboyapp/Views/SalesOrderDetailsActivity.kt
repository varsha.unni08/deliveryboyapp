package com.centroid.deliveryboyapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.adapter.SalesOrderItemAdapter
import com.centroid.deliveryboyapp.adapter.SalesUserItemAdapter
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.domain.SalesOrderItem
import com.centroid.deliveryboyapp.domain.SalesUser
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.google.android.gms.maps.SupportMapFragment
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_order_details.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class SalesOrderDetailsActivity : AppCompatActivity() {

    var order: SalesOrder?=null

    var salesUser: SalesUser?=null

    var closed=0
    var pl:List<SalesOrderItem>?=null

    var hourlyTask: TimerTask?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales_order_details)
        supportActionBar!!.hide()

        order=intent.getSerializableExtra("Order") as SalesOrder

        txtOrderid.setText("Order Id : "+order!!.cdSalesOrder)


        address.setText(order!!.dsDeliveryAddress)
        if(order!!.dsLocation!=null&&!order!!.dsLocation.equals("")) {
            pincode.setText(order!!.dsLocation)

        }
        else{

            pincode.setText("Not available")

        }

        amount.setText(order!!.vlTotal.toString()+" " +this.getString(R.string.rs))
        ServiceCharge.setText(order!!.vlDeliveryCharge.toString()+" "  +this.getString(R.string.rs))
        var a=order!!.vlTotal.toDouble()+order!!.vlDeliveryCharge.toDouble()
        totalamount.setText(a.toString()+" " +this.getString(R.string.rs))
        if(order!!.cdTransactionType.equals("0")) {
            txtTransactiontype.setText("Cash on delivery")
        }
        else{

            txtTransactiontype.setText("Online")
        }
        getUserDetails()

        getOrderItems()

        carddrop.setOnClickListener {


            if(closed==0)
            {
                closed=1

                imgDropdown.setImageResource(R.drawable.ic_arrow_drop_up)

                recycler_view_items.visibility=View.VISIBLE
            }
            else{

                closed=0

                imgDropdown.setImageResource(R.drawable.ic_arrow_drop_down)

                recycler_view_items.visibility=View.GONE
            }


        }


        btnpick.setOnClickListener {


            var isAllChecked=true

            if(pl!=null) {

                for (i in pl!!) {
                    if (i.checked == 0) {
                        isAllChecked = false

                        break
                    }
                }
            }

            if(isAllChecked)
            {


                var p: ProgressFragment = ProgressFragment()
                p.show(supportFragmentManager, "dhjjd")


                val retrofithelper = Retrofithelper()

                var a: Call<JsonObject> = retrofithelper.getClient().changeCheckedStatus(
                    "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdSalesOrder
                );

                a.enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        p.dismiss()

                        if (response.body() != null) {

                          try{


                              var js=JSONObject(response.body().toString())

                              if(js.getInt("status")==1)
                              {

                                  Toast.makeText(this@SalesOrderDetailsActivity,"you checked all items",Toast.LENGTH_SHORT).show()
                                  startActivity(Intent(this@SalesOrderDetailsActivity,BillPrintActivity::class.java).putExtra("salesorder",order!!.cdSalesOrder))

                              }
                              else{

                                  Toast.makeText(this@SalesOrderDetailsActivity,"failed",Toast.LENGTH_SHORT).show()

                              }



                          }catch (e:Exception)
                          {

                          }


                        }


                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                        p.dismiss()

                        if (!DownloadUtils().isNetworkConnected(this@SalesOrderDetailsActivity)) {
                            DownloadUtils().showNoConnectiondialog(this@SalesOrderDetailsActivity);
                        }

                        t.printStackTrace()


                    }
                })




            }
            else{

                Toast.makeText(this,"Please check all items",Toast.LENGTH_SHORT).show()

            }

        }




    }


    fun getOrderItems()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<List<SalesOrderItem>> = retrofithelper.getClient().getSalesUserSalesOrderItems(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdSalesOrder
        );

        a.enqueue(object : Callback<List<SalesOrderItem>> {
            override fun onResponse(call: Call<List<SalesOrderItem>>, response: Response<List<SalesOrderItem>>) {

                p.dismiss()

                if (response.body() != null) {

                     pl = response.body();

                    recycler_view_items.layoutManager= LinearLayoutManager(this@SalesOrderDetailsActivity)
                    recycler_view_items.adapter= SalesUserItemAdapter(this@SalesOrderDetailsActivity,pl!!)



                }


            }

            override fun onFailure(call: Call<List<SalesOrderItem>>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@SalesOrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@SalesOrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }


    fun getUserDetails()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<SalesUser> = retrofithelper.getClient().getSalesUserDetails(
            "Bearer "+ PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdUser
        );

        a.enqueue(object : Callback<SalesUser> {
            override fun onResponse(call: Call<SalesUser>, response: Response<SalesUser>) {

                p.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    salesUser=p;

                    name.setText(p!!.dsCustomer)
                    contact.setText(p!!.dsPhoneno)



                }


            }

            override fun onFailure(call: Call<SalesUser>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@SalesOrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@SalesOrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }
}
