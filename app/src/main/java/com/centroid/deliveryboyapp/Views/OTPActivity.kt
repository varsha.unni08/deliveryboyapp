package com.centroid.deliveryboyapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_otp.*

class OTPActivity : AppCompatActivity() {

    var token:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        supportActionBar!!.hide()

        token=intent.getStringExtra("token");

        val randomPIN = (Math.random() * 9000).toInt() + 1000

        var a=randomPIN.toString()


        Toast.makeText(this, ""+randomPIN, Toast.LENGTH_SHORT).show()


        btnSendOtp.setOnClickListener {


            if(a.equals(edtOTP.text.toString()))
            {
                PreferenceHelper(this).putData(Constants.tokenkey, token)

                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }





        }
    }
}
