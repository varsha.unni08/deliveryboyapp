package com.centroid.deliveryboyapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sales_login.*
import kotlinx.android.synthetic.main.activity_sales_login.btnlogin
import kotlinx.android.synthetic.main.activity_sales_login.edtPassword
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class SalesLoginActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales_login)
        supportActionBar!!.hide()

        btnlogin.setOnClickListener {

            if(!edtUser.text.toString().equals(""))
            {
                if(!edtPassword.text.toString().equals(""))
                {


                    checkLogin()



                }
                else{
                    Toast.makeText(this,"Enter password",Toast.LENGTH_SHORT).show()
                }


            }
            else{
                Toast.makeText(this,"Enter username",Toast.LENGTH_SHORT).show()
            }

        }
    }


    fun checkLogin()
    {
        try {

            var p: ProgressFragment = ProgressFragment()
            p.show(supportFragmentManager, "dhjjd")


            val retrofithelper = Retrofithelper()

            var a: Call<JsonObject> = retrofithelper.getClient().checkLoginSales(
                edtUser.text.toString().trim(),edtPassword.text.toString()
            )


            a.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    p.dismiss()

                    if (response.body() != null) {

                        var p = response.body();

                        var jsonobj= JSONObject(p.toString());

                        if (jsonobj.getInt("status")==1) {


                            var token=  jsonobj.getString("token")

                            PreferenceHelper(this@SalesLoginActivity).putData(Constants.tokenkey, token)


                            startActivity(Intent(this@SalesLoginActivity, SaleDetailsActivity::class.java).putExtra("token",token))
                            finish()


                        } else {

                            Toast.makeText(this@SalesLoginActivity, "Login failed", Toast.LENGTH_SHORT).show()


                        }


                    }


                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    p.dismiss()

                    if (!DownloadUtils().isNetworkConnected(this@SalesLoginActivity)) {
                        DownloadUtils().showNoConnectiondialog(this@SalesLoginActivity);
                    }

                    t.printStackTrace()


                }
            })


        }catch (ex: Exception)
        {

        }

    }
}
