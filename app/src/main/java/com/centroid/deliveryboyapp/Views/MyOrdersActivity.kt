package com.centroid.deliveryboyapp.Views

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.adapter.OrderHistoryAdapter
import com.centroid.deliveryboyapp.adapter.SalesOrderAdapter
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.activity_my_orders.*
import kotlinx.android.synthetic.main.activity_my_orders.imgback
import kotlinx.android.synthetic.main.activity_my_orders.recycler_view
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService

import com.centroid.deliveryboyapp.adapter.OrderDateAdapter
import com.centroid.deliveryboyapp.domain.OrderDate
import com.centroid.deliveryboyapp.progress.ProgressFragment
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService

import java.lang.Exception
import java.text.SimpleDateFormat


class MyOrdersActivity : AppCompatActivity() {

    var stdate=""

    var endDate=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)

        supportActionBar!!.hide()

        imgback.setOnClickListener {


            onBackPressed()
        }

        txtStartDate.setOnClickListener {

            showdatepicker(1)
        }

        imgstdate.setOnClickListener {

            showdatepicker(1)
        }


        txtEndDate.setOnClickListener {
            showdatepicker(0)

        }

        imgEddate.setOnClickListener {

            showdatepicker(0)
        }

        btnSubmit.setOnClickListener {


            if(!stdate.equals("")&&!endDate.equals(""))
            {
                showLatestOrders()

            }
            else{

                Toast.makeText(this,"Select start date and end date",Toast.LENGTH_SHORT).show()
            }

        }

       // showLatestOrders()
    }




    fun showdatepicker(st:Int)
    {


        val cal= Calendar.getInstance();

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
//                cal.set(Calendar.YEAR, year)
//                cal.set(Calendar.MONTH, monthOfYear)
//                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//                updateDateInView()

                var m=monthOfYear+1;

                var a=""


                if(m<10)
                {
                  a="0"+m.toString()
                }
                else{

                    a=m.toString()
                }

                if(st==1)
                {

//                    stdate=""+dayOfMonth+"-"+m+"-"+year

                    stdate=""+year+"-"+a+"-"+dayOfMonth

try {
    val format = SimpleDateFormat("yyyy-MM-dd")
    val date = format.parse(stdate)


    val format_req = SimpleDateFormat("dd-MM-yyyy")

    val d = format_req.format(date)

    txtStartDate.setText(d.toString())
}catch (Ex:Exception)
{

}




                }
                else{

                    endDate=""+year+"-"+a+"-"+dayOfMonth



                    try {
                        val format = SimpleDateFormat("yyyy-MM-dd")
                        val date = format.parse(endDate)


                        val format_req = SimpleDateFormat("dd-MM-yyyy")

                        val d = format_req.format(date)

                        txtEndDate.setText(d.toString())
                    }catch (Ex:Exception)
                    {

                    }


                }







            }
        }



        DatePickerDialog(this,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)).show()
    }


    fun showLatestOrders()
    {
        var data="1";



       var status=sp_ordeStatus.selectedItem as String

        if(status.equals("Initial"))
        {
            data="1"
        }
        if(status.equals("Dispatched"))
        {
            data="2"
        }
        if(status.equals("cancelled"))
        {
            data="3"
        }
        if(status.equals("Delivered"))
        {
            data="5"
        }


var progressFragment=ProgressFragment()
        progressFragment.show(supportFragmentManager,"")





        val retrofithelper = Retrofithelper()

        var a: Call<List<SalesOrder>> = retrofithelper.getClient().getOrderHistory(
            "Bearer "+ PreferenceHelper(this).getData(Constants.tokenkey),
                    stdate,endDate,data);

        a.enqueue(object : Callback<List<SalesOrder>> {
            override fun onResponse(call: Call<List<SalesOrder>>, response: Response<List<SalesOrder>>) {

progressFragment.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    if(p!!.size>0)
                    {

                        val set = HashSet<String>()

                        for (a in p)
                        {
                            set.add(a.dtDelivery)
                        }


                        var dt=ArrayList<OrderDate>()

                        val list = ArrayList(set)

                        for (i in list)
                        {

                            var d=OrderDate()
                            d.date=i
                            d.selected=0

                            var so=ArrayList<SalesOrder>()


                            for (a in p)
                            {

                                if(i.equals(a.dtDelivery.trim())) {
                                    so.add(a)
                                }
                            }

                            d.salesOrders=so


                            dt.add(d)

                        }


                        recycler_view.layoutManager=LinearLayoutManager(this@MyOrdersActivity)

                        var salesOrderAdapter= OrderDateAdapter(dt,this@MyOrdersActivity)
                        recycler_view.adapter=salesOrderAdapter





                    }






                }
                else{


                }


            }

            override fun onFailure(call: Call<List<SalesOrder>>, t: Throwable) {

                progressFragment.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@MyOrdersActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@MyOrdersActivity);
                }

                t.printStackTrace()


            }
        })
    }

}
