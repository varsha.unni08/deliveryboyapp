package com.centroid.deliveryboyapp.Views

import android.app.*
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.advertise.quickjob.app.webservicehelper.Retrofithelper
import com.centroid.deliveryboyapp.Constants.Constants
import com.centroid.deliveryboyapp.Constants.DownloadUtils
import com.centroid.deliveryboyapp.R
import com.centroid.deliveryboyapp.domain.SalesOrder
import com.centroid.deliveryboyapp.domain.SalesUser
import com.centroid.deliveryboyapp.preferencehelper.PreferenceHelper
import com.centroid.deliveryboyapp.progress.ProgressFragment
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.list_itemorders.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.media.MediaPlayer
import androidx.core.content.ContextCompat.getSystemService

import android.net.Uri
import android.os.Build
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.centroid.deliveryboyapp.adapter.SalesOrderItemAdapter
import com.centroid.deliveryboyapp.domain.SalesOrderItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*


class OrderDetailsActivity : AppCompatActivity(),OnMapReadyCallback {

    var order:SalesOrder?=null

    var salesUser:SalesUser?=null

    var closed=0

    var hourlyTask:TimerTask?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        supportActionBar!!.hide()


        var data= PreferenceHelper(this).getData(Constants.CURRENTORDER);

        var gson=GsonBuilder().create()
        order=gson.fromJson<SalesOrder>(data,SalesOrder::class.java)

        txtOrderid.setText("Order Id : "+order!!.cdSalesOrder)


address.setText(order!!.dsDeliveryAddress)

        if(order!!.dsLocation!=null&&!order!!.dsLocation.equals("")) {
            pincode.setText(order!!.dsLocation)

            val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
            mapFragment!!.getMapAsync(this)
        }
        else{

            pincode.setText("Not available")

            card_location.visibility=View.GONE
        }
     amount.setText(order!!.vlTotal.toString()+" " +this.getString(R.string.rs))
     ServiceCharge.setText(order!!.vlDeliveryCharge.toString()+" "  +this.getString(R.string.rs))
        var a=order!!.vlTotal.toDouble()+order!!.vlDeliveryCharge.toDouble()
        totalamount.setText(a.toString()+" " +this.getString(R.string.rs))
        if(order!!.cdTransactionType.equals("0")) {
         txtTransactiontype.setText("Cash on delivery")
        }
        else{

         txtTransactiontype.setText("Online")
        }

        getUserDetails()
        getOrderItems()
        imgback.setOnClickListener {

            finishAffinity()
            System.exit(0)

        }

        fabcall.setOnClickListener {

if(salesUser!=null) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:" + salesUser!!.dsPhoneno)
    startActivity(intent)
}
        }

        closed=0

        carddrop.setOnClickListener {


            if(closed==0)
            {
                closed=1

                imgDropdown.setImageResource(R.drawable.ic_arrow_drop_up)

                recycler_view_items.visibility=View.VISIBLE
            }
            else{

                closed=0

                imgDropdown.setImageResource(R.drawable.ic_arrow_drop_down)

                recycler_view_items.visibility=View.GONE
            }


        }



        btnpick.setOnClickListener {

            if(order!!.cdTransactionType.equals("0")) {
                //txtTransactiontype.setText("Cash on delivery")



                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.app_name)
                builder.setMessage("Did you collect cash ?")
                builder.setNegativeButton(
                    "yes"
                ) {


                        dialog, which -> dialog.dismiss()

                    changeOrderStatus()


                }


                builder.setPositiveButton(
                    "no"
                ) {


                        dialog, which -> dialog.dismiss()


                }
                val alertDialog = builder.create()
                alertDialog.show()







            }
            else{

                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.app_name)
                builder.setMessage("Customer already paid the amount via online")
                builder.setNegativeButton(
                    "Ok"
                ) {


                        dialog, which -> dialog.dismiss()

                    changeOrderStatus()


                }



                val alertDialog = builder.create()
                alertDialog.show()
















            }






        }

        val timer = Timer()
         hourlyTask = object : TimerTask() {
            override  fun run() {


                checkCurrentOrderDetails()

            }
        }


        timer.schedule (hourlyTask, 2000, 3000);

    }

    override fun onBackPressed() {
//        super.onBackPressed()

        finishAffinity()
        System.exit(0)
    }



    fun checkCurrentOrderDetails()
    {

        val retrofithelper = Retrofithelper()

        var a: Call<SalesOrder> = retrofithelper.getClient().checkOrderStatusCancelled(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdSalesOrder
        );


        a.enqueue(object : Callback<SalesOrder> {
            override fun onResponse(call: Call<SalesOrder>, response: Response<SalesOrder>) {



                if (response.body() != null) {

                    var p = response.body();

                  if(p!!.cdSalesOrderStatus.toInt()==3)
                  {


                      sendPushNotification("Sorry Unfortunately customer cancelled the order")
                      hourlyTask!!.cancel()

                      PreferenceHelper(this@OrderDetailsActivity).putData(Constants.CURRENTORDER,"")

                      startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))


                      finish()

                  }


                }


            }

            override fun onFailure(call: Call<SalesOrder>, t: Throwable) {



                if (!DownloadUtils().isNetworkConnected(this@OrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@OrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })

    }


    private fun sendPushNotification(msg: String) {

        try {


            val m = (Date().time / 1000L % Integer.MAX_VALUE).toInt()


            val CHANNEL_ID = "my_channel_01"// The id of the channel.
            val name = "mychannel"// The user-visible name of the channel.
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel: NotificationChannel
            // Create a notification and set the notification channel.

            var notification: Notification? = null

            val mNotificationManager: NotificationManager

            val intent = Intent(this@OrderDetailsActivity, SplashActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            //            builder.setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification = Notification.Builder(this@OrderDetailsActivity, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)


                    .setChannelId(CHANNEL_ID)
                    .setStyle(Notification.BigTextStyle().bigText(msg))
                    .build()

                mChannel = NotificationChannel(CHANNEL_ID, name, importance)
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager.createNotificationChannel(mChannel)

            } else {

                notification = Notification.Builder(this@OrderDetailsActivity)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setStyle(Notification.BigTextStyle().bigText(msg))

                    .build()
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }


            // Issue the notification.
            mNotificationManager.notify(m, notification)


            val mp: MediaPlayer
            mp = MediaPlayer.create(this@OrderDetailsActivity, R.raw.notifysound)
            mp.setOnCompletionListener { mp ->
                var mp = mp
                // TODO Auto-generated method stub
                mp!!.reset()
                mp!!.release()
                mp = null
            }
            mp.start()


   
            //
            //
        } catch (e: Exception) {

        }


    }
    
    
    fun changeOrderStatus()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<JsonObject> = retrofithelper.getClient().changeDeliveryStatus(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdSalesOrder
        );



        a.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                p.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    var jsonobj=JSONObject(p.toString());

                    if (jsonobj.getInt("status")==1) {


                        val gson = Gson()
                        val jsonString = gson.toJson(order)


                        PreferenceHelper(this@OrderDetailsActivity).putData(Constants.CURRENTORDER,"")
                        startActivity(Intent(this@OrderDetailsActivity, MainActivity::class.java))



                    } else {

                        Toast.makeText(this@OrderDetailsActivity, "failed", Toast.LENGTH_SHORT).show()


                    }


                }


            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@OrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@OrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }
    
    
    
    
    
    
    
    
    
    
    



    fun getOrderItems()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<List<SalesOrderItem>> = retrofithelper.getClient().getSalesOrderItems(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdSalesOrder
        );

        a.enqueue(object : Callback<List<SalesOrderItem>> {
            override fun onResponse(call: Call<List<SalesOrderItem>>, response: Response<List<SalesOrderItem>>) {

                p.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    recycler_view_items.layoutManager=LinearLayoutManager(this@OrderDetailsActivity)
                    recycler_view_items.adapter=SalesOrderItemAdapter(this@OrderDetailsActivity,p!!)



                }


            }

            override fun onFailure(call: Call<List<SalesOrderItem>>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@OrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@OrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }








    fun getUserDetails()
    {
        var p: ProgressFragment = ProgressFragment()
        p.show(supportFragmentManager, "dhjjd")


        val retrofithelper = Retrofithelper()

        var a: Call<SalesUser> = retrofithelper.getClient().getUserDetails(
            "Bearer "+PreferenceHelper(this).getData(Constants.tokenkey),order!!.cdUser
        );

        a.enqueue(object : Callback<SalesUser> {
            override fun onResponse(call: Call<SalesUser>, response: Response<SalesUser>) {

                p.dismiss()

                if (response.body() != null) {

                    var p = response.body();

                    salesUser=p;

              name.setText(p!!.dsCustomer)
                    contact.setText(p!!.dsPhoneno)



                }


            }

            override fun onFailure(call: Call<SalesUser>, t: Throwable) {

                p.dismiss()

                if (!DownloadUtils().isNetworkConnected(this@OrderDetailsActivity)) {
                    DownloadUtils().showNoConnectiondialog(this@OrderDetailsActivity);
                }

                t.printStackTrace()


            }
        })
    }


    override fun onMapReady(p0: GoogleMap?) {
        val sydney = LatLng(order!!.dsLatitude.toDouble(), order!!.dsLongitude.toDouble())
        p0!!.addMarker(
            MarkerOptions()
                .position(sydney)
                .title(order!!.dsLocation)


        );


        p0!!.animateCamera( CameraUpdateFactory.newLatLngZoom(
            LatLng(
                order!!.dsLatitude.toDouble(), order!!.dsLongitude.toDouble()
            ), 16.0f))

        p0!!.setOnMapClickListener {

            var s="https://maps.google.com/?q="+order!!.dsLatitude+","+order!!.dsLongitude


            var gmmIntentUri = Uri.parse(s);

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
val mapIntent =  Intent(Intent.ACTION_VIEW, gmmIntentUri);
// Make the Intent explicit by setting the Google Maps package


// Attempt to start an activity that can handle the Intent
startActivity(mapIntent);




        }

    }
}
