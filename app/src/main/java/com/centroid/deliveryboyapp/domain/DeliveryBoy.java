package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryBoy {

    @SerializedName("cd_delivery")
    @Expose
    private String cdDelivery;
    @SerializedName("cd_user")
    @Expose
    private String cdUser;
    @SerializedName("ds_boy_name")
    @Expose
    private String dsBoyName;
    @SerializedName("ds_address")
    @Expose
    private String dsAddress;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("ds_user")
    @Expose
    private String dsUser;
    @SerializedName("ds_password")
    @Expose
    private String dsPassword;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cd_pincode")
    @Expose
    private String cdPincode;
    @SerializedName("cd_region")
    @Expose
    private String cdRegion;
    @SerializedName("assign_status")
    @Expose
    private String assignStatus;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;


    public DeliveryBoy() {
    }

    public String getCdDelivery() {
        return cdDelivery;
    }

    public void setCdDelivery(String cdDelivery) {
        this.cdDelivery = cdDelivery;
    }

    public String getCdUser() {
        return cdUser;
    }

    public void setCdUser(String cdUser) {
        this.cdUser = cdUser;
    }

    public String getDsBoyName() {
        return dsBoyName;
    }

    public void setDsBoyName(String dsBoyName) {
        this.dsBoyName = dsBoyName;
    }

    public String getDsAddress() {
        return dsAddress;
    }

    public void setDsAddress(String dsAddress) {
        this.dsAddress = dsAddress;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDsUser() {
        return dsUser;
    }

    public void setDsUser(String dsUser) {
        this.dsUser = dsUser;
    }

    public String getDsPassword() {
        return dsPassword;
    }

    public void setDsPassword(String dsPassword) {
        this.dsPassword = dsPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCdPincode() {
        return cdPincode;
    }

    public void setCdPincode(String cdPincode) {
        this.cdPincode = cdPincode;
    }

    public String getCdRegion() {
        return cdRegion;
    }

    public void setCdRegion(String cdRegion) {
        this.cdRegion = cdRegion;
    }

    public String getAssignStatus() {
        return assignStatus;
    }

    public void setAssignStatus(String assignStatus) {
        this.assignStatus = assignStatus;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }
}
