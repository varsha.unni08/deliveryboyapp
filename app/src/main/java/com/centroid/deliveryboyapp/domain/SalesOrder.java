package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SalesOrder implements Serializable {

    @SerializedName("status")
    @Expose
    private String status="1";
    @SerializedName("cd_sales_order")
    @Expose
    private String cdSalesOrder;
    @SerializedName("ds_sales_order")
    @Expose
    private String dsSalesOrder;
    @SerializedName("ds_combo_offer")
    @Expose
    private String dsComboOffer;
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch;
    @SerializedName("dt_sales_order")
    @Expose
    private String dtSalesOrder;
    @SerializedName("dt_delivery")
    @Expose
    private String dtDelivery;
    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer;
    @SerializedName("cd_user")
    @Expose
    private String cdUser;
    @SerializedName("ds_delivery_address")
    @Expose
    private String dsDeliveryAddress;
    @SerializedName("cd_pincode")
    @Expose
    private String cdPincode;
    @SerializedName("cd_region")
    @Expose
    private String cdRegion;
    @SerializedName("vl_total")
    @Expose
    private String vlTotal;
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal;
    @SerializedName("vl_discount")
    @Expose
    private String vlDiscount;
    @SerializedName("ds_free_delivery_status")
    @Expose
    private String dsFreeDeliveryStatus;
    @SerializedName("vl_delivery_charge")
    @Expose
    private String vlDeliveryCharge;
    @SerializedName("cd_sales_order_status")
    @Expose
    private String cdSalesOrderStatus;
    @SerializedName("cd_delivery_boy")
    @Expose
    private String cdDeliveryBoy;
    @SerializedName("ds_latitude")
    @Expose
    private String dsLatitude;
    @SerializedName("ds_longitude")
    @Expose
    private String dsLongitude;
    @SerializedName("ds_location")
    @Expose
    private String dsLocation;
    @SerializedName("ds_landmark")
    @Expose
    private String dsLandmark;
    @SerializedName("cd_payment_status")
    @Expose
    private String cdPaymentStatus;
    @SerializedName("ds_transaction_id")
    @Expose
    private String dsTransactionId;
    @SerializedName("cd_refund_status")
    @Expose
    private String cdRefundStatus;
    @SerializedName("cd_transaction_type")
    @Expose
    private String cdTransactionType;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;
    @SerializedName("cd_checked")
    @Expose
    private String cd_checked;

    public SalesOrder() {
    }

    public String getCd_checked() {
        return cd_checked;
    }

    public void setCd_checked(String cd_checked) {
        this.cd_checked = cd_checked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCdSalesOrder() {
        return cdSalesOrder;
    }

    public void setCdSalesOrder(String cdSalesOrder) {
        this.cdSalesOrder = cdSalesOrder;
    }

    public String getDsSalesOrder() {
        return dsSalesOrder;
    }

    public void setDsSalesOrder(String dsSalesOrder) {
        this.dsSalesOrder = dsSalesOrder;
    }

    public String getDsComboOffer() {
        return dsComboOffer;
    }

    public void setDsComboOffer(String dsComboOffer) {
        this.dsComboOffer = dsComboOffer;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDtSalesOrder() {
        return dtSalesOrder;
    }

    public void setDtSalesOrder(String dtSalesOrder) {
        this.dtSalesOrder = dtSalesOrder;
    }

    public String getDtDelivery() {
        return dtDelivery;
    }

    public void setDtDelivery(String dtDelivery) {
        this.dtDelivery = dtDelivery;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getCdUser() {
        return cdUser;
    }

    public void setCdUser(String cdUser) {
        this.cdUser = cdUser;
    }

    public String getDsDeliveryAddress() {
        return dsDeliveryAddress;
    }

    public void setDsDeliveryAddress(String dsDeliveryAddress) {
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public String getCdPincode() {
        return cdPincode;
    }

    public void setCdPincode(String cdPincode) {
        this.cdPincode = cdPincode;
    }

    public String getCdRegion() {
        return cdRegion;
    }

    public void setCdRegion(String cdRegion) {
        this.cdRegion = cdRegion;
    }

    public String getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(String vlTotal) {
        this.vlTotal = vlTotal;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }

    public String getVlDiscount() {
        return vlDiscount;
    }

    public void setVlDiscount(String vlDiscount) {
        this.vlDiscount = vlDiscount;
    }

    public String getDsFreeDeliveryStatus() {
        return dsFreeDeliveryStatus;
    }

    public void setDsFreeDeliveryStatus(String dsFreeDeliveryStatus) {
        this.dsFreeDeliveryStatus = dsFreeDeliveryStatus;
    }

    public String getVlDeliveryCharge() {
        return vlDeliveryCharge;
    }

    public void setVlDeliveryCharge(String vlDeliveryCharge) {
        this.vlDeliveryCharge = vlDeliveryCharge;
    }

    public String getCdSalesOrderStatus() {
        return cdSalesOrderStatus;
    }

    public void setCdSalesOrderStatus(String cdSalesOrderStatus) {
        this.cdSalesOrderStatus = cdSalesOrderStatus;
    }

    public String getCdDeliveryBoy() {
        return cdDeliveryBoy;
    }

    public void setCdDeliveryBoy(String cdDeliveryBoy) {
        this.cdDeliveryBoy = cdDeliveryBoy;
    }

    public String getDsLatitude() {
        return dsLatitude;
    }

    public void setDsLatitude(String dsLatitude) {
        this.dsLatitude = dsLatitude;
    }

    public String getDsLongitude() {
        return dsLongitude;
    }

    public void setDsLongitude(String dsLongitude) {
        this.dsLongitude = dsLongitude;
    }

    public String getDsLocation() {
        return dsLocation;
    }

    public void setDsLocation(String dsLocation) {
        this.dsLocation = dsLocation;
    }

    public String getDsLandmark() {
        return dsLandmark;
    }

    public void setDsLandmark(String dsLandmark) {
        this.dsLandmark = dsLandmark;
    }

    public String getCdPaymentStatus() {
        return cdPaymentStatus;
    }

    public void setCdPaymentStatus(String cdPaymentStatus) {
        this.cdPaymentStatus = cdPaymentStatus;
    }

    public String getDsTransactionId() {
        return dsTransactionId;
    }

    public void setDsTransactionId(String dsTransactionId) {
        this.dsTransactionId = dsTransactionId;
    }

    public String getCdRefundStatus() {
        return cdRefundStatus;
    }

    public void setCdRefundStatus(String cdRefundStatus) {
        this.cdRefundStatus = cdRefundStatus;
    }

    public String getCdTransactionType() {
        return cdTransactionType;
    }

    public void setCdTransactionType(String cdTransactionType) {
        this.cdTransactionType = cdTransactionType;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }
}
