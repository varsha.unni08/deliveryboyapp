package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesUser {

    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer;
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch;
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer;
    @SerializedName("cd_user")
    @Expose
    private String cdUser;
    @SerializedName("ds_address")
    @Expose
    private String dsAddress;
    @SerializedName("ds_phoneno")
    @Expose
    private String dsPhoneno;
    @SerializedName("ds_delivery_address")
    @Expose
    private String dsDeliveryAddress;
    @SerializedName("ds_pincode")
    @Expose
    private String dsPincode;
    @SerializedName("cd_last_delivered_region")
    @Expose
    private Object cdLastDeliveredRegion;
    @SerializedName("vl_advance")
    @Expose
    private String vlAdvance;
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;

    public SalesUser() {
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getCdUser() {
        return cdUser;
    }

    public void setCdUser(String cdUser) {
        this.cdUser = cdUser;
    }

    public String getDsAddress() {
        return dsAddress;
    }

    public void setDsAddress(String dsAddress) {
        this.dsAddress = dsAddress;
    }

    public String getDsPhoneno() {
        return dsPhoneno;
    }

    public void setDsPhoneno(String dsPhoneno) {
        this.dsPhoneno = dsPhoneno;
    }

    public String getDsDeliveryAddress() {
        return dsDeliveryAddress;
    }

    public void setDsDeliveryAddress(String dsDeliveryAddress) {
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public String getDsPincode() {
        return dsPincode;
    }

    public void setDsPincode(String dsPincode) {
        this.dsPincode = dsPincode;
    }

    public Object getCdLastDeliveredRegion() {
        return cdLastDeliveredRegion;
    }

    public void setCdLastDeliveredRegion(Object cdLastDeliveredRegion) {
        this.cdLastDeliveredRegion = cdLastDeliveredRegion;
    }

    public String getVlAdvance() {
        return vlAdvance;
    }

    public void setVlAdvance(String vlAdvance) {
        this.vlAdvance = vlAdvance;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }
}
