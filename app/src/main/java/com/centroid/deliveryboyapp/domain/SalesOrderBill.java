package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SalesOrderBill {

    @SerializedName("cd_sales_order")
    @Expose
    private String cdSalesOrder;
    @SerializedName("ds_sales_order")
    @Expose
    private String dsSalesOrder;
    @SerializedName("ds_combo_offer")
    @Expose
    private String dsComboOffer;
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch;
    @SerializedName("dt_sales_order")
    @Expose
    private String dtSalesOrder;
    @SerializedName("dt_delivery")
    @Expose
    private String dtDelivery;
    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer;
    @SerializedName("cd_user")
    @Expose
    private String cdUser;
    @SerializedName("ds_delivery_address")
    @Expose
    private String dsDeliveryAddress;
    @SerializedName("cd_pincode")
    @Expose
    private String cdPincode;
    @SerializedName("cd_region")
    @Expose
    private String cdRegion;
    @SerializedName("vl_total")
    @Expose
    private String vlTotal;
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal;
    @SerializedName("vl_discount")
    @Expose
    private String vlDiscount;
    @SerializedName("vl_delivery_charge")
    @Expose
    private String vlDeliveryCharge;
    @SerializedName("user")
    @Expose
    private SalesUser user;
    @SerializedName("salesorderitem")
    @Expose
    private List<SalesItems> salesorderitem = new ArrayList<>();

    public SalesOrderBill() {
    }

    public String getCdSalesOrder() {
        return cdSalesOrder;
    }

    public void setCdSalesOrder(String cdSalesOrder) {
        this.cdSalesOrder = cdSalesOrder;
    }

    public String getDsSalesOrder() {
        return dsSalesOrder;
    }

    public void setDsSalesOrder(String dsSalesOrder) {
        this.dsSalesOrder = dsSalesOrder;
    }

    public String getDsComboOffer() {
        return dsComboOffer;
    }

    public void setDsComboOffer(String dsComboOffer) {
        this.dsComboOffer = dsComboOffer;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDtSalesOrder() {
        return dtSalesOrder;
    }

    public void setDtSalesOrder(String dtSalesOrder) {
        this.dtSalesOrder = dtSalesOrder;
    }

    public String getDtDelivery() {
        return dtDelivery;
    }

    public void setDtDelivery(String dtDelivery) {
        this.dtDelivery = dtDelivery;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getCdUser() {
        return cdUser;
    }

    public void setCdUser(String cdUser) {
        this.cdUser = cdUser;
    }

    public String getDsDeliveryAddress() {
        return dsDeliveryAddress;
    }

    public void setDsDeliveryAddress(String dsDeliveryAddress) {
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public String getCdPincode() {
        return cdPincode;
    }

    public void setCdPincode(String cdPincode) {
        this.cdPincode = cdPincode;
    }

    public String getCdRegion() {
        return cdRegion;
    }

    public void setCdRegion(String cdRegion) {
        this.cdRegion = cdRegion;
    }

    public String getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(String vlTotal) {
        this.vlTotal = vlTotal;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }

    public String getVlDiscount() {
        return vlDiscount;
    }

    public void setVlDiscount(String vlDiscount) {
        this.vlDiscount = vlDiscount;
    }

    public String getVlDeliveryCharge() {
        return vlDeliveryCharge;
    }

    public void setVlDeliveryCharge(String vlDeliveryCharge) {
        this.vlDeliveryCharge = vlDeliveryCharge;
    }

    public SalesUser getUser() {
        return user;
    }

    public void setUser(SalesUser user) {
        this.user = user;
    }

    public List<SalesItems> getSalesorderitem() {
        return salesorderitem;
    }

    public void setSalesorderitem(List<SalesItems> salesorderitem) {
        this.salesorderitem = salesorderitem;
    }
}
