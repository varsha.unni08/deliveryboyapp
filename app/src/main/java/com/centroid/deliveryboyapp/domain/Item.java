package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch;
    @SerializedName("ds_item_code")
    @Expose
    private String dsItemCode;
    @SerializedName("ds_item")
    @Expose
    private String dsItem;
    @SerializedName("cd_item_master")
    @Expose
    private String cdItemMaster;
    @SerializedName("cd_item_brand")
    @Expose
    private String cdItemBrand;
    @SerializedName("cd_main_category")
    @Expose
    private String cdMainCategory;
    @SerializedName("cd_category")
    @Expose
    private String cdCategory;
    @SerializedName("cd_subcategory")
    @Expose
    private String cdSubcategory;
    @SerializedName("nr_openingstock")
    @Expose
    private String nrOpeningstock;
    @SerializedName("cd_uom")
    @Expose
    private String cdUom;
    @SerializedName("vl_mrp")
    @Expose
    private String vlMrp;
    @SerializedName("vl_selling_price")
    @Expose
    private String vlSellingPrice;
    @SerializedName("vl_offer_status")
    @Expose
    private String vlOfferStatus;
    @SerializedName("vl_offer_description")
    @Expose
    private String vlOfferDescription;
    @SerializedName("ds_hsncode")
    @Expose
    private String dsHsncode;
    @SerializedName("ds_item_description")
    @Expose
    private String dsItemDescription;
    @SerializedName("ds_item_pic")
    @Expose
    private String dsItemPic;
    @SerializedName("ds_item_pic_small")
    @Expose
    private String dsItemPicSmall;
    @SerializedName("ds_tax_status")
    @Expose
    private String dsTaxStatus;
    @SerializedName("vl_cgst")
    @Expose
    private String vlCgst;
    @SerializedName("vl_sgst")
    @Expose
    private String vlSgst;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;

    public Item() {
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsItemCode() {
        return dsItemCode;
    }

    public void setDsItemCode(String dsItemCode) {
        this.dsItemCode = dsItemCode;
    }

    public String getDsItem() {
        return dsItem;
    }

    public void setDsItem(String dsItem) {
        this.dsItem = dsItem;
    }

    public String getCdItemMaster() {
        return cdItemMaster;
    }

    public void setCdItemMaster(String cdItemMaster) {
        this.cdItemMaster = cdItemMaster;
    }

    public String getCdItemBrand() {
        return cdItemBrand;
    }

    public void setCdItemBrand(String cdItemBrand) {
        this.cdItemBrand = cdItemBrand;
    }

    public String getCdMainCategory() {
        return cdMainCategory;
    }

    public void setCdMainCategory(String cdMainCategory) {
        this.cdMainCategory = cdMainCategory;
    }

    public String getCdCategory() {
        return cdCategory;
    }

    public void setCdCategory(String cdCategory) {
        this.cdCategory = cdCategory;
    }

    public String getCdSubcategory() {
        return cdSubcategory;
    }

    public void setCdSubcategory(String cdSubcategory) {
        this.cdSubcategory = cdSubcategory;
    }

    public String getNrOpeningstock() {
        return nrOpeningstock;
    }

    public void setNrOpeningstock(String nrOpeningstock) {
        this.nrOpeningstock = nrOpeningstock;
    }

    public String getCdUom() {
        return cdUom;
    }

    public void setCdUom(String cdUom) {
        this.cdUom = cdUom;
    }

    public String getVlMrp() {
        return vlMrp;
    }

    public void setVlMrp(String vlMrp) {
        this.vlMrp = vlMrp;
    }

    public String getVlSellingPrice() {
        return vlSellingPrice;
    }

    public void setVlSellingPrice(String vlSellingPrice) {
        this.vlSellingPrice = vlSellingPrice;
    }

    public String getVlOfferStatus() {
        return vlOfferStatus;
    }

    public void setVlOfferStatus(String vlOfferStatus) {
        this.vlOfferStatus = vlOfferStatus;
    }

    public String getVlOfferDescription() {
        return vlOfferDescription;
    }

    public void setVlOfferDescription(String vlOfferDescription) {
        this.vlOfferDescription = vlOfferDescription;
    }

    public String getDsHsncode() {
        return dsHsncode;
    }

    public void setDsHsncode(String dsHsncode) {
        this.dsHsncode = dsHsncode;
    }

    public String getDsItemDescription() {
        return dsItemDescription;
    }

    public void setDsItemDescription(String dsItemDescription) {
        this.dsItemDescription = dsItemDescription;
    }

    public String getDsItemPic() {
        return dsItemPic;
    }

    public void setDsItemPic(String dsItemPic) {
        this.dsItemPic = dsItemPic;
    }

    public String getDsItemPicSmall() {
        return dsItemPicSmall;
    }

    public void setDsItemPicSmall(String dsItemPicSmall) {
        this.dsItemPicSmall = dsItemPicSmall;
    }

    public String getDsTaxStatus() {
        return dsTaxStatus;
    }

    public void setDsTaxStatus(String dsTaxStatus) {
        this.dsTaxStatus = dsTaxStatus;
    }

    public String getVlCgst() {
        return vlCgst;
    }

    public void setVlCgst(String vlCgst) {
        this.vlCgst = vlCgst;
    }

    public String getVlSgst() {
        return vlSgst;
    }

    public void setVlSgst(String vlSgst) {
        this.vlSgst = vlSgst;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }
}
