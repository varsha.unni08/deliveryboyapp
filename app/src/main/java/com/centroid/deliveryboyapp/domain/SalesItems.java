package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesItems {

    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("nr_qty")
    @Expose
    private String nrQty;
    @SerializedName("vl_total")
    @Expose
    private String vlTotal;
    @SerializedName("item")
    @Expose
    private Item item;

    public SalesItems() {
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getNrQty() {
        return nrQty;
    }

    public void setNrQty(String nrQty) {
        this.nrQty = nrQty;
    }

    public String getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(String vlTotal) {
        this.vlTotal = vlTotal;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
