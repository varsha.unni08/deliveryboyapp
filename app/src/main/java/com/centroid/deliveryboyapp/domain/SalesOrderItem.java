package com.centroid.deliveryboyapp.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesOrderItem {


    @SerializedName("cd_sales_order_item")
    @Expose
    private String cdSalesOrderItem;
    @SerializedName("Item Name")
    @Expose
    private String itemName;
    @SerializedName("nr_qty")
    @Expose
    private String nrQty;
    @SerializedName("vl_net_total")
    @Expose
    private String vlNetTotal;
    @SerializedName("item_image")
    @Expose
    private String itemImage;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;
    @SerializedName("cd_refunded")
    @Expose
    private String cdRefunded;

    @SerializedName("unitprice")
    @Expose
    private String unitprice;

    int checked=0;

    public SalesOrderItem() {
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    public String getCdSalesOrderItem() {
        return cdSalesOrderItem;
    }

    public void setCdSalesOrderItem(String cdSalesOrderItem) {
        this.cdSalesOrderItem = cdSalesOrderItem;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getNrQty() {
        return nrQty;
    }

    public void setNrQty(String nrQty) {
        this.nrQty = nrQty;
    }

    public String getVlNetTotal() {
        return vlNetTotal;
    }

    public void setVlNetTotal(String vlNetTotal) {
        this.vlNetTotal = vlNetTotal;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }

    public String getCdRefunded() {
        return cdRefunded;
    }

    public void setCdRefunded(String cdRefunded) {
        this.cdRefunded = cdRefunded;
    }
}
