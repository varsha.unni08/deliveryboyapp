package com.centroid.deliveryboyapp.domain;

import java.util.ArrayList;
import java.util.List;

public class OrderDate {


    String date;
    int selected=0;

    List<SalesOrder>salesOrders=new ArrayList<>();

    public OrderDate() {
    }

    public List<SalesOrder> getSalesOrders() {
        return salesOrders;
    }

    public void setSalesOrders(List<SalesOrder> salesOrders) {
        this.salesOrders = salesOrders;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
