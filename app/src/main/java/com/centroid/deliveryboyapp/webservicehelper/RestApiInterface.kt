package com.advertise.quickjob.app.webservicehelper


import com.centroid.deliveryboyapp.domain.*
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RestApiInterface {


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkLoginSales.php")
    fun checkLoginSales(@Field("username")username:String,@Field("password")password:String
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("changeCheckedStatus.php")
    fun changeCheckedStatus(@Header("Token")Token:String,@Field("orderid")orderid:String
    ): Call<JsonObject>




    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("UserLogin.php")
    fun UserLogin(@Field("mobile")mobile:String
                    ): Call<JsonObject>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getUserDetails.php")
    fun getUserDetails(@Header("Token")Token:String, @Field("cd_user")cd_user:String
    ): Call<SalesUser>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getSalesUserDetails.php")
    fun getSalesUserDetails(@Header("Token")Token:String, @Field("cd_user")cd_user:String
    ): Call<SalesUser>



    @Headers("Accept: application/json")
    @GET("getDeliveryBoyProfile.php")
    fun getDeliveryBoyProfile(@Header("Token")Token:String
    ): Call<DeliveryBoy>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getSalesOrderItems.php")
    fun getSalesOrderItems(@Header("Token")Token:String, @Field("cd_sales_order")cd_sales_order:String
    ): Call<List<SalesOrderItem>>



    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getSalesUserSalesOrderItems.php")
    fun getSalesUserSalesOrderItems(@Header("Token")Token:String, @Field("cd_sales_order")cd_sales_order:String
    ): Call<List<SalesOrderItem>>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getSalesOrderFullDetails.php")
    fun getSalesOrderFullDetails(@Header("Token")Token:String, @Field("cd_sales_order")cd_sales_order:String
    ): Call<SalesOrderBill>





    @Headers("Accept: application/json")
    @POST("getOrders.php")
    fun getOrders(@Header("Token")token:String
    ): Call<List<SalesOrder>>




    @Headers("Accept: application/json")
    @POST("getSalesOrder.php")
    fun getSalesOrder(@Header("Token")token:String
    ): Call<List<SalesOrder>>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("checkOrderStatusCancelled.php")
    fun checkOrderStatusCancelled(@Header("Token")token:String,@Field("orderid")orderid:String
    ): Call<SalesOrder>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("getOrderHistory.php")
    fun getOrderHistory(@Header("Token")token:String,@Field("dtstart")dtstart:String,
                        @Field("dtend")dtend:String,@Field("status")status:String

    ): Call<List<SalesOrder>>







    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("ChangepickupStatus.php")
    fun ChangepickupStatus(@Header("Token")token:String,@Field("orderid")orderid:String,@Field("pickupstatus")pickupstatus:String
    ): Call<JsonObject>


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("changeDeliveryStatus.php")
    fun changeDeliveryStatus(@Header("Token")token:String,@Field("orderid")orderid:String
    ): Call<JsonObject>






}