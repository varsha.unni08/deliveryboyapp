package com.advertise.quickjob.app.webservicehelper


import com.centroid.deliveryboyapp.Constants.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Retrofithelper {




    fun getClient(): RestApiInterface {


            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)

            val builder = OkHttpClient.Builder()
            builder.interceptors().add(logging)

            val httpClient = builder.build()

            val client = Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()

          var  RestApiInterface = client.create(RestApiInterface::class.java!!)

        return RestApiInterface
    }



}